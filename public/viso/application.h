
#ifndef VISO_APPLICATION_H_
#define VISO_APPLICATION_H_

#include <interface/application.h>
#include <viso/model.h>
#include <viso/recorder.h>
#include <base/taskmanager.h>
#include <thread/pool.h>
#include "common.h"

namespace Viso {

class Application : public Interface::Application
{
private:
    using ColorModel = Graphics::RGBf;
    Recorder<ColorModel> recorder;
    
    Base::TaskManager m_manager;
    Thread::Pool m_pool;
    
    Model model;
public:
    Application();
    virtual ~Application();

    virtual bool init() override;

    virtual int run() override;
};

} // ns Viso

#endif // VISO_APPLICATION_H_
