
#ifndef VISO_MODEL_H_
#define VISO_MODEL_H_

#include "common.h"
#include "ray.h"
#include <graphics/color.h>
#include <viso/resource.h>
#include <viso/scene/scene.h>
#include <viso/animation/animation.h>

namespace Viso {

class Model
{
private:
    Resource m_resources;
    Animation m_animation;
    Scene m_scene;
public:
    Model();
    ~Model();

    Resource& getResource();
    Animation& getAnimation();
    Scene& getScene();

    Graphics::RGBf cast(const Ray& ray);

    void update(Time delta);
    void commit();
};

} // ns Viso

#endif // VISO_MODEL_H_
