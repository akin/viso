
#ifndef VISO_RENDERBLOCK_H_
#define VISO_RENDERBLOCK_H_

#include "common.h"
#include <graphics/rectangle.h>

namespace Viso {

class RenderBlock
{
private:
    Graphics::Rectangle<size_t> m_rectangle;
    // Matrix to transform pixels into world space
    // Matrix to transform pixels into directions
    // Camera? reference?
    // Destination
public:
    RenderBlock()
    {
    }

    RenderBlock(const RenderBlock& other)
    : m_rectangle(other.m_rectangle)
    {
    }

    ~RenderBlock()
    {
    }

    Graphics::Rectangle<size_t>& getRectangle()
    {
        return m_rectangle;
    }

    const Graphics::Rectangle<size_t>& getRectangle() const
    {
        return m_rectangle;
    }
};

} // ns Viso

#endif // VISO_RENDERBLOCK_H_
