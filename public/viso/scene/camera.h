
#ifndef VISO_CAMERA_H_
#define VISO_CAMERA_H_

#include "node.h"

namespace Viso {

class Camera : public Node
{
private:
    Matrix m_projection;
    Float m_near = 0.1;
    Float m_far = 100.0;
public:
    Camera();
    virtual ~Camera();

    void setLookAt(glm::vec3 pos);

    void setNear(Float value);
    void setFar(Float value);

    void setupPerspective(Float hfov, Float width, Float height);
    void setupFrustum(Float left, Float right, Float top, Float bottom);
    void setupOrtho(Float left, Float right, Float top, Float bottom);

    Float getNear() const;
    Float getFar() const;

    const Matrix& getProjection() const;
};

} // ns Viso

#endif // VISO_CAMERA_H_
