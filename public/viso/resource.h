
#ifndef VISO_RESOURCE_H_
#define VISO_RESOURCE_H_

#include "common.h"

namespace Viso {

class Resource
{
public:
    Resource();
    ~Resource();
};

} // ns Viso

#endif // VISO_RESOURCE_H_
