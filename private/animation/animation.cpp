
#include <viso/animation/animation.h>

namespace Viso {

Animation::Animation()
{
}

void Animation::add(Event *event)
{
    m_events.push_back(event);
}

void Animation::add(Process *process)
{
    m_proc.push_back(process);
}

void Animation::seek(Time time)
{
    m_time = time;
}

std::string Animation::getTimestamp() const
{
    auto str = std::to_string(m_time);

    return str;
}

void Animation::update(Time delta)
{
    m_time += delta;

    for(auto process : m_proc)
    {
        process->update(m_time);
    }
}

} // ns Viso
