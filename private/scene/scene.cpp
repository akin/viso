
#include <viso/scene/scene.h>

namespace Viso {

Scene::Scene()
{
    openvdb::initialize();
    m_grid = openvdb::FloatGrid::create();
}

Scene::~Scene()
{
}

void Scene::add(Node* node)
{
    m_nodes.push_back(node);
}

void Scene::commit()
{
    for(auto node : m_nodes)
    {
        node->commit();
    }
}

} // ns Viso
