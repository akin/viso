
#include <viso/scene/node.h>

namespace Viso {

Node::Node()
: m_scale{1.0f,1.0f,1.0f}
{
}

Node::~Node()
{
}

void Node::setPosition(glm::vec3 value)
{
    m_position.x = value.x;
    m_position.y = value.y;
    m_position.z = value.z;
    m_dirty = true;
}

void Node::setScale(glm::vec3 value)
{
    m_scale = value;
    m_dirty = true;
}

void Node::setOrientation(glm::quat value)
{
    m_orientation = value;
    m_dirty = true;
}

const std::string& Node::getName() const
{
    return m_name;
}

void Node::setName(const std::string& value)
{
    m_name = value;
}

void Node::commit()
{
    if(!m_dirty)
    {
        return;
    }
    // Calculate world matrix.
    static glm::mat4 identity = glm::mat4(1.0);
    glm::mat4 translationMatrix = glm::translate(identity, m_position);
    glm::mat4 scaleMatrix = glm::scale(identity, m_scale);
    glm::mat4 rotationMatrix = glm::mat4_cast(m_orientation);

    m_transform = translationMatrix * rotationMatrix * scaleMatrix;

    m_dirty = false;
}

const Matrix& Node::getTransform() const
{

    return m_transform;
}

} // ns Viso
